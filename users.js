var basis = require('alpi_basis');
var filters = require('alpi_filters');

module.exports = {
	/**
     * List all users.
     * 
	 * @param {Boolean} detailed
     * @param {Function} callback
     */
	List: function (detailed, callback) {
		basis.System.ShellCommand('awk -F: \'{ print $1 }\' /etc/passwd', (users) => {
			if(users.error === null){
				users = users.stdout.trim().split('\n');
				if(detailed){
					module.exports.Informations(users, (informations)=>{
						if(informations !== false){
							callback(informations);
						}else{
							callback(false);
						}
					});
				}else{
					callback(users);
				}
			}else{
				callback(false);
			}
		});
	},
	/**
     * List all users created by the system.
     * @param {Boolean} detailed
     * @param {Function} callback
     */
	ListSystemUsers: function (detailed, callback) {
		basis.System.ShellCommand('awk -F: \'$3 < 1000 { print $1 }\' /etc/passwd', (systemUsers) => {
			if (systemUsers.error === null) {
				systemUsers = systemUsers.stdout.trim().split('\n');
				if (detailed) {
					module.exports.Informations(systemUsers, (informations) => {
						if (informations !== false) {
							callback(informations);
						} else {
							callback(false);
						}
					});
				} else {
					callback(systemUsers);
				}
			} else {
				callback(false);
			}
		});
	},
	/**
     * List all users not created by the system.
     * 
	 * @param {Boolean} detailed
     * @param {Function} callback
     */
	ListNonSystemUsers: function (detailed, callback) {
		basis.System.ShellCommand('awk -F: \'$3 >= 1000 { print $1 }\' /etc/passwd', (nonSystemUsers) => {
			if (nonSystemUsers.error === null) {
				nonSystemUsers = nonSystemUsers.stdout.trim().split('\n');
				if (detailed) {
					module.exports.Informations(nonSystemUsers, (informations) => {
						if (informations !== false) {
							callback(informations);
						} else {
							callback(false);
						}
					});
				} else {
					callback(nonSystemUsers);
				}
			} else {
				callback(false);
			}
		});
	},
	/**
     * Check if the user in parameter exists.
     * 
     * @param {String|String[]} username
     * @param {Function} callback
     */
	Exists: function (username, callback) {
		//Get all users
		module.exports.List(false, (users) => {
			if (Array.isArray(username)) {
				//if username is an array create another array with the username as key and if it exists in value.
				var result = [];
				username.forEach((u) => {
					result[u] = users.includes(u.trim());
				});
				callback(result);
			} else {
				callback((typeof users !== 'boolean') ? users.includes(username.trim()) : false);
			}
		});
	},

	Informations: function (username, callback) {
		module.exports.Exists(username, (exists) => {
			var idUsername = username;
			var grepPattern = `^${username}`;
			if (Array.isArray(username)) {
				if (exists.every((e) => { return exists[e] === true; })) {
					exists = true;
					idUsername = username.join(' ');
					grepPattern = `^${username.join('\\|^')}`;
				} else {
					exists === false;
				}
			}
			if (exists === true) {
				basis.System.ShellCommand(`id ${idUsername}; grep '${grepPattern}' /etc/passwd | cut -d':' -f7`, (informations) => {
					if (informations.error === null) {
						informations = informations.stdout.trim().split('\n');
						var shells = informations.splice(informations.length / 2, informations.length - 1);
						var result = [];
						for (let i = 0; i < informations.length; i++) {
							var information = informations[i].split(' ');
							var uid = information[0].split('=')[1];
							var gid = information[1].split('=')[1];
							var groups = information[2].split('=')[1];
							var shell = shells[i];

							uid = {
								id: uid.split('(')[0],
								name: uid.split('(')[1].split(')')[0]
							};

							gid = {
								id: gid.split('(')[0],
								name: gid.split('(')[1].split(')')[0]
							};

							groups = groups.split(',').map((g) => {
								return {
									id: g.split('(')[0],
									name: g.split('(')[1].split(')')[0]
								};
							});

							result.push({
								uid: uid,
								gid: gid,
								groups: groups,
								shell: shell
							});
						}
						callback(result);
					} else {
						callback(false);
					}
				});
			} else {
				callback(false);
			}
		});
	},
	/**
	 * Add a new user
	 * 
	 * @param {String} username
	 * @param {String} password
	 * @param {String} primaryGroup
	 * @param {String[]} secondaryGroups
	 * @param {String} shell
	 * @param {Function} callback
	 */
	Add: function (username, password, primaryGroup, secondaryGroups, shell, callback) {
		if (filters.Validate.Username(username) === true) {
			//check if the user exists
			module.exports.Exists(username, (exists) => {
				if (exists === false) {
					//check if the shell exists
					require('./shells').Exists(shell, (exists) => {
						if (exists === true) {
							var command = '';
							//check if the primary group exists
							require('./groups').Exists(primaryGroup, (exists) => {
								if (exists === true) {
									new Promise((resolve) => {
										//check if the secondary groups are given
										if (secondaryGroups.length > 0) {
											//check if the secondary groups exists
											require('./groups').Exists(secondaryGroups, (exists) => {
												if (exists === true) {
													command = 'sudo useradd -m -g ' + primaryGroup + ' -G ' + secondaryGroups.join(',') + ' -s ' + shell + ' ' + username;
													resolve(true);
												} else {
													resolve(false);
												}
											});
										} else {
											command = 'sudo useradd -m -g ' + primaryGroup + ' -s ' + shell + ' ' + username;
											resolve(true);
										}
									}).then((result) => {
										if (result === true) {
											//create the user using one of the two commands above
											basis.System.ShellCommand(command, (e) => {
												if (String(e.stderr) === '') {
													password = filters.Escape.Backslash(filters.Escape.Quotes(password));
													//change the user's password
													basis.System.ShellCommand('echo \'' + username + ':' + password + '\' | sudo chpasswd', (e) => {
														if (String(e.stderr) === '') {
															//check if the created user exists
															module.exports.Exists(username, (exists) => {
																callback(exists);
															});
														} else {
															callback(false);
														}
													});
												} else {
													callback(false);
												}
											});
										} else {
											callback(false);
										}
									});
								} else {
									callback(false);
								}
							});
						} else {
							callback(false);
						}
					});
				} else {
					callback(false);
				}
			});
		} else {
			callback(false);
		}
	},
	/**
	 * Delete the user in parameter.
	 * 
	 * @param {String} username
	 * @param {String} password
	 * @param {Boolean} deleteFiles
	 * @param {Function} callback
	 */
	Delete: function (username, password, deleteFiles, callback) {
		if (filters.Validate.Username(username) === true) {
			//check if the user exists
			module.exports.Exists(username, (exists) => {
				if (exists === true) {
					//check user's password
					module.exports.CheckPassword(username, password, (valid) => {
						if (valid === true) {
							var command = (deleteFiles === true) ? 'sudo userdel -r ' + username : 'sudo userdel ' + username + ' && sudo mv /home/' + username + ' /home/' + username + '.deleted';
							basis.System.ShellCommand(command, (e) => {
								callback(e.error === null);
							});
						} else {
							callback(false);
						}
					});
				} else {
					callback(false);
				}
			});
		} else {
			callback(false);
		}
	},
	/**
	 * Check the password of the user in parameter
	 * 
	 * @param {String} username
	 * @param {String} password
	 * @param {Function} callback
	 */
	CheckPassword: function (username, password, callback) {
		if (filters.Validate.Username(username) === true) {
			module.exports.Exists(username, (exists) => {
				if (exists === true) {
					password = filters.Escape.Backslash(filters.Escape.Quotes(password));
					basis.System.ShellCommand('echo \'' + password + '\' | su ' + username + ' -s /usr/bin/whoami', (e) => {
						callback(String(e.stdout).trim() === username);
					});
				} else {
					callback(false);
				}
			});
		} else {
			callback(false);
		}
	},
	/**
	 * Change the password of the user in parameter
	 * 
	 * @param {String} username
	 * @param {String} currentPassword
	 * @param {String} newPassword
	 * @param {Boolean|Object} randomPassword false or {size: number, numbers: boolean, capitalize: boolean, symbols: boolean}
	 * @param {Function} callback 
	 */
	ChangePassword: function (username, currentPassword, newPassword, randomPassword, callback) {
		if (filters.Validate.Username(username) === true) {
			//check if the user exists
			module.exports.Exists(username, (exists) => {
				if (exists === true) {
					//check the user's current password
					module.exports.CheckPassword(username, currentPassword, (correct) => {
						if (correct === true) {
							new Promise((resolve) => {
								if (typeof randomPassword !== 'boolean') {
									//generate new random password
									basis.Other.RandomString(randomPassword.size, randomPassword.numbers, randomPassword.capitalize, randomPassword.symbols, (generatedPassword) => {
										if (typeof generatedPassword !== 'boolean') {
											newPassword = generatedPassword;
											resolve(true);
										} else {
											resolve(false);
										}
									});
								} else {
									//check if the new password is different from the old one
									if (currentPassword != newPassword) {
										resolve(true);
									} else {
										callback(true);
										return;
									}
								}
							}).then((result) => {
								//if everything went well
								if (result === true) {
									newPassword = filters.Escape.Backslash(filters.Escape.Quotes(newPassword));
									basis.System.ShellCommand('echo \'' + username + ':' + newPassword + '\' | sudo chpasswd', (e) => {
										callback(String(e.stderr) === '' ? newPassword : false);
									});
								} else {
									callback(false);
								}
							});
						} else {
							callback(false);
						}
					});
				} else {
					callback(false);
				}
			});
		} else {
			callback(false);
		}
	},
	/**
	 * Modify the user in parameter.
	 * 
	 * @param {String} username
	 * @param {String} currentPassword
	 * @param {String} newPassword
	 * @param {Boolean|Object} randomPassword false or {size: number, numbers: boolean, capitalize: boolean, symbols: boolean}
	 * @param {String} primaryGroup
	 * @param {String[]} secondaryGroups
	 * @param {String} shell
	 * @param {Function} callback
	 */
	ModifyUser: function (username, currentPassword, newPassword, randomPassword, primaryGroup, secondaryGroups, shell, callback) {
		if (filters.Validate.Username(username) === true) {
			module.exports.ChangePassword(username, currentPassword, newPassword, randomPassword, (passwordChanged) => {
				if (passwordChanged === true || typeof passwordChanged === 'string') {
					if (typeof passwordChanged === 'string') {
						newPassword = passwordChanged;
					}
					require('./groups').ChangeUserPrimaryGroup(username, newPassword, primaryGroup, (changed) => {
						if (changed === true) {
							require('./groups').ChangeUserSecondaryGroups(username, newPassword, secondaryGroups, (changed) => {
								if (changed === true) {
									require('./shells').ChangeUserShell(username, newPassword, shell, (changed) => {
										if (typeof passwordChanged === 'string') {
											callback(changed === true ? newPassword : false);
										} else {
											callback(changed);
										}
									});
								} else {
									callback(false);
								}
							});
						} else {
							callback(false);
						}
					});
				} else {
					callback(false);
				}
			});
		} else {
			callback(false);
		}
	}
};
