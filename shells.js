var basis = require('alpi_basis');
var filters = require('alpi_filters');

module.exports = {

	/**
     * List all shells.
     * 
     * @param {Function} callback
     */
	List: function (callback) {
		basis.System.ShellCommand('grep bin /etc/shells', (e) => {
			callback(String(e.stderr) === '' ? String(e.stdout).trim().split('\n') : false);
		});
	},
	/**
     * Check if the shell in parameter exists.
     * 
     * @param {String} shell
     * @param {Function} callback
     */
	Exists: function (shell, callback) {
		module.exports.List((shells) => {
			callback((typeof shells !== 'boolean') ? shells.includes(shell) : false);
		});
	},
	/**
     * Get the user shell.
     * 
     * @param {String} username
     * @param {Function} callback 
     */
	UserShell: function (username, callback) {
		if (filters.Validate.Username(username) === true) {
			//check if the user exists
			require('./users').Exists(username, (exists) => {
				if (exists === true) {
					//get the user's shell
					basis.System.ShellCommand('getent passwd ' + username + ' | cut -d: -f7', (e) => {
						if (String(e.stderr) === '') {
							var shell = String(e.stdout).trim();
							//check if the shell exists
							module.exports.Exists(shell, (e) => {
								callback((e === true) ? shell : false);
							});
						} else {
							callback(false);
						}
					});
				} else {
					callback(false);
				}
			});
		} else {
			callback(false);
		}
	},

	/**
     * Change the user's shell.
     * 
     * @param {String} username
     * @param {String} password
     * @param {String} shell
     * @param {Function} callback
     */
	ChangeUserShell: function (username, password, shell, callback) {
		if (filters.Validate.Username(username) === true) {
			//check if the user exists
			require('./users').Exists(username, (exists) => {
				if (exists === true) {
					shell = filters.Other.path(shell).trim();
					//check if the shell exists
					module.exports.Exists(shell, (exists) => {
						if (exists === true) {
							//get the user's shell
							module.exports.UserShell(username, (currentShell) => {
								if (currentShell != shell) {
									//if they are different, change to the new one
									basis.System.ShellCommand('sudo chsh -s ' + shell + ' ' + username, (e) => {
										callback(String(e.stderr) === '');
									});
								} else {
									callback(true);
								}
							});
						} else {
							callback(false);
						}
					});
				} else {
					callback(false);
				}
			});
		} else {
			callback(false);
		}
	}
};
