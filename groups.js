var basis = require('alpi_basis');
var filters = require('alpi_filters');

module.exports = {
	/**
     * List all groups.
     * 
	 * @param {Boolean} detailed
     * @param {Function} callback
     */
	List: function (detailed, callback) {
		basis.System.ShellCommand(`cut -d':' -f1${detailed ? ',3':''} /etc/group`, (groups) => {
			if(groups.error === null){
				groups = groups.stdout.trim().split('\n');
				if(detailed){
					var result = [];
					for(let i = 0; i < groups.length; i++){
						var group = groups[i].split(':');
						result.push({
							id: group[1],
							name: group[0]
						});
					}
					callback(result);
				}else{
					callback(groups);
				}
			}else{
				callback(false);
			}
		});
	},
	/**
     * Check if the group in parameter exists.
     * 
     * @param {String|String[]} group
     * @param {Function} callback
     */
	Exists: function (group, callback) {
		//Get all groups
		module.exports.List(false, (groups) => {
			if (Array.isArray(group)) {
				//if group is an array create another array with the group name as key and if it exists in value
				var result = [];
				group.forEach((g) => {
					result[g] = groups.includes(g.trim());
				});
				callback(result);
			} else {
				callback((groups !== false) ? groups.includes(group.trim()) : false);
			}
		});
	},
	/**
     * Create the group in parameter.
     * 
     * @param {String} group
     * @param {Function} callback
     */
	Create: function (group, callback) {
		//check if the group exists
		module.exports.Exists(group, (exists) => {
			if (exists === false) {
				//create it if it does not exist
				basis.System.ShellCommand('sudo groupadd ' + group, (e) => {
					callback(e.stderr === '');
				});
			} else {
				callback(false);
			}
		});
	},
	/**
     * Delete the group in parameter.
     * 
     * @param {String} group
     * @param {Function} callback
     */
	Delete: function (group, callback) {
		//check if the group exists
		module.exports.Exists(group, (exists) => {
			if (exists === true) {
				//delete it if it exists
				basis.System.ShellCommand('sudo groupdel ' + group, (e) => {
					callback(e.stderr === '');
				});
			} else {
				callback(false);
			}
		});
	},
	/**
     * Get user's primary group.
     * 
     * @param {String} username
     * @param {Function} callback
     */
	UserPrimaryGroup: function (username, callback) {
		if (filters.Validate.Username(username) === true) {
			//check if the user exists
			require('./users').Exists(username, (exists) => {
				if (exists === true) {
					basis.System.ShellCommand('id -gn ' + username, (primaryGroup) => {
						callback(primaryGroup.stderr === '' ? primaryGroup.stdout.trim() : false);
					});
				} else {
					callback(false);
				}
			});
		} else {
			callback(false);
		}
	},
	/**
     * Get user's secondary groups.
     * 
     * @param {String} username
     * @param {Function} callback
     */
	UserSecondaryGroups: function (username, callback) {
		if (filters.Validate.Username(username) === true) {
			//check if the user exists
			require('./users').Exists(username, (exists) => {
				if (exists === true) {
					basis.System.ShellCommand('id -Gn ' + username, (secondaryGroups) => {
						callback(secondaryGroups.stderr === '' ? secondaryGroups.stdout.trim().split(' ') : false);
					});
				} else {
					callback(false);
				}
			});
		} else {
			callback(false);
		}
	},
	/**
     * Change user's primary group.
     * 
     * @param {String} username
     * @param {String} password
     * @param {String} primaryGroup
     * @param {Function} callback
     */
	ChangeUserPrimaryGroup: function (username, password, primaryGroup, callback) {
		if (filters.Validate.Username(username) === true) {
			//check if the user exists
			require('./users').Exists(username, (exists) => {
				if (exists === true) {
					//check user's password
					require('./users').CheckPassword(username, password, (valid) => {
						if (valid === true) {
							//check if the primaryGroup exists
							module.exports.Exists(primaryGroup, (exists) => {
								if (exists === true) {
									//get user's primary group
									module.exports.UserPrimaryGroup(username, (currentPrimaryGroup) => {
										if (currentPrimaryGroup != primaryGroup) {
											//if they are differents change to the new one
											basis.System.ShellCommand('sudo usermod -g ' + primaryGroup + ' ' + username, (e) => {
												callback(e.stderr === '');
											});
										} else {
											callback(true);
										}
									});
								} else {
									callback(false);
								}
							});
						} else {
							callback(false);
						}
					});
				} else {
					callback(false);
				}
			});
		} else {
			callback(false);
		}
	},
	/**
     * Change user's secondary groups.
     * 
     * @param {String} username
     * @param {String} password
     * @param {String[]} secondaryGroups
     * @param {Function} callback
     */
	ChangeUserSecondaryGroups: function (username, password, secondaryGroups, callback) {
		if (filters.Validate.Username(username) === true) {
			//check if the user exists
			require('./users').Exists(username, (exists) => {
				if (exists === true) {
					//check user's password
					require('./users').CheckPassword(username, password, (valid) => {
						if (valid === true) {
							//check if groups exists
							module.exports.Exists(secondaryGroups, (e) => {
								var exists = true;
								e.forEach(group => {
									if (e[group] === false) {
										exists = false;
									}
								});
								if (exists === true) {
									module.exports.UserSecondaryGroups(username, (currentSecondaryGroups) => {
										var differents = false;
										//check if they are all the same
										if (currentSecondaryGroups.length == secondaryGroups.length) {
											secondaryGroups.forEach(group => {
												if (!currentSecondaryGroups.includes(group.trim())) {
													differents = true;
												}
											});
										} else {
											differents = true;
										}
										// @ts-ignore
										if (differents === true) {
											basis.System.ShellCommand('sudo usermod -G ' + secondaryGroups.join(',') + ' ' + username, (e) => {
												callback(e.stderr === '');
											});
										} else {
											callback(true);
										}
									});
								} else {
									callback(false);
								}
							});
						} else {
							callback(false);
						}
					});
				} else {
					callback(false);
				}
			});
		} else {
			callback(false);
		}
	},
	/**
     * add a group to the user's secondary groups without asking for a password.
     * 
     * @param {String} username
     * @param {String} group
     * @param {Function} callback
     */
	AddUserSecondaryGroupWithoutPassword: function (username, group, callback) {
		if (filters.Validate.Username(username) === true) {
			module.exports.Exists(group, (exists) => {
				if (exists === true) {
					module.exports.UserSecondaryGroups(username, (secondaryGroups) => {
						if (secondaryGroups !== false) {
							if (secondaryGroups.includes(group) === false) {
								secondaryGroups.push(group);
								basis.System.ShellCommand('sudo usermod -G ' + secondaryGroups.join(',') + ' ' + username, (e) => {
									callback(e.stderr === '');
								});
							} else {
								callback(false);
							}
						} else {
							callback(false);
						}
					});
				} else {
					callback(false);
				}
			});
		} else {
			callback(false);
		}
	},
	/**
     * Check if a user belong to a group.
     * 
     * @param {String} username
     * @param {String} group
     * @param {Function} callback
     */
	UserBelongToGroup: function (username, group, callback) {
		module.exports.Exists(group, (exists) => {
			if (exists === true) {
				module.exports.UserPrimaryGroup(username, (primaryGroup) => {
					if (primaryGroup !== false) {
						module.exports.UserSecondaryGroups(username, (secondaryGroups) => {
							if (secondaryGroups !== false) {
								if (group === primaryGroup || secondaryGroups.includes(group)) {
									callback(true);
								} else {
									callback(false);
								}
							} else {
								callback(false);
							}
						});
					} else {
						callback(false);
					}
				});
			} else {
				callback(false);
			}
		});
	}
};
